
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title></title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">
</head>

<body>

        <div class="col-12 text-center">
            <div id="question" class="mt-2">
              <div id="question-form" class="panel panel-default">
			   <span style="color:red">You can Submit the questions in the below question box during the Q&A Session.</span>
                  <form method="POST" action="#" class="form panel-body" role="form">
                      <div class="row">
                          <div class="col-12">
                          <div id="ques-message"></div>
                          <div class="form-group">
                             <input type="email"class="form-control" name="email" id="userQuestion" required placeholder="Please ask your question" rows="1">
                          </div>
                          <div class="form-group">
                              <textarea class="form-control" name="userQuestion" id="userQuestion" required placeholder="Please ask your question" rows="2"></textarea>
                          </div>
                          
                          </div>
                          <div class="col-12">
            
                          <button class="btn btn-primary btn-sm btn-submit" type="submit">Submit your Question</button>
                          </div>
                      </div>
                      
                      
                      
                  </form>
              </div>
          </div>
            
        </div>
    </div>
</div>

<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
$(function(){


	$(document).on('submit', '#question-form form', function()
    {  
            $.post('submitques.php', $(this).serialize(), function(data)
            {
                if(data=="success")
                {
                  $('#ques-message').text('Your question is submitted successfully.');
                  $('#ques-message').removeClass('alert-danger').addClass('alert-success').fadeIn().delay(2000).fadeOut();
                  $('#question-form').find("textarea").val('');
                }
                else 
                {
                  $('#ques-message').text(data);
                  $('#ques-message').removeClass('alert-success').addClass('alert-danger').fadeIn().delay(5000).fadeOut();
                }
                
            });
        
      
      return false;
    });
});
function update()
{
    $.ajax({ url: 'ajax.php',
         data: {action: 'update'},
         type: 'post',
         success: function(output) {
			   if(output=="0")
			   {
				   location.href='index.php';
			   }
         }
});
}
setInterval(function(){ update(); }, 30000);

function changeVideo(video, l)
{
    var vid = '.'+l;
    $('.vid-link').removeClass('act');
    $(vid).addClass('act');
    
    $('#webcast').attr("src",video);
    
    return false;
    
}

</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-93480057-11"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-93480057-11');
</script>

</body>
</html>