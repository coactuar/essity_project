<?php
require_once "config.php";
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Poll Results</title>
<link rel="stylesheet" type="text/css" href="assects/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="assects/css/styles.css">
</head>

<body class="no-bg">
<div class="container-fluid">
    <div class="row pollques border">
        <div class="col-12">
            <h6>Poll Results</h6>
            <form>
            <div id="pollresults"></div>
            </form>
        </div>
     </div>   
</div>

<script src="assects/js/jquery.min.js"></script>
<script src="assects/js/bootstrap.min.js"></script>
<script language="javascript">
$(function(){
    getPollResults("<?php echo $_GET['id']; ?>");
});

function getPollResults(id)
{
    $.ajax({
        url: 'ajax.php',
        data: {action: 'getpollresults', pid: id},
        type: 'post',
        success: function(response) {
            
            $("#pollresults").html(response);
            
        }
    });
    
}
setInterval(function(){ getPollResults('<?php echo $_GET['id']; ?>'); }, 10000);
</script>
</body>
</html>