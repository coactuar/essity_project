<?php
require_once "../functions/config.php";
           
if(isset($_POST['action']) && !empty($_POST['action'])) {
    
    $action = $_POST['action'];
    
    switch($action) {
		
		 case 'getusers':
		 
 if (isset($_POST["page"])) 
            { 
                $page  = $_POST["page"]; 
            }
            else { 
                $page=1; 
            }
            
            $start_from = ($page-1) * $limit;
        
            $sql = "SELECT COUNT(id) as count FROM tbl_users";  
            $rs_result = mysqli_query($link, $sql) or die(mysqli_error($link)); 
            $row = mysqli_fetch_assoc($rs_result);
            $total_records = $row['count'];  
            $total_pages = ceil($total_records / $limit);
            ?>
            <div class="row user-info">
                <div class="col-6">
                    Total Users: <?php echo $total_records; ?>
                </div>
            </div> 
            <div class="row user-details">
                <div class="col-12">
                    <table class="table table-striped table-light">
                      <thead class="thead-inverse">
                        <tr>
                          <!-- <th>Title</th> -->
                          <th>First name</th>
                          <th>Last name</th>
                          <th>Email</th>
                          <th>Number</th>
                          <th>Hospital/Organization</th>
                          <th>specialty</th>
                          <!-- <th>Country</th>
                          <th>State</th>
                          <th>city</th> -->
                          <th>experience</th>
                          <th>Check box</th>
                          <th>Register on</th>
                          <th>Last Login On</th>
                          <th>Logout On</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php		
                        $query="select * from tbl_users ";
                        $res = mysqli_query($link, $query) or die(mysqli_error($link));
                        while($data = mysqli_fetch_assoc($res))
                        {
                        ?>
                          <tr>
                            <!-- <td><?php echo $data['title']; ?></td> -->
                            <td><?php echo $data['first_name']; ?></td>
                            <td><?php echo $data['last_name']; ?></td>
                            <td><?php echo $data['emailid']; ?></td>
                            <td><?php echo $data['phone_num']; ?></td>
							
                            <td><?php echo $data['updates']; ?></td>
                            
							<td><?php echo $data['topic_interest']; ?></td>
              <!-- <td><?php echo $data['country']; ?></td>
              <td><?php echo $data['state']; ?></td>
              <td><?php echo $data['city']; ?></td> -->
                            <td><?php echo $data['specialty']; ?></td>
                            <td><?php echo $data['checked']; ?></td>

                            <td><?php echo $data['reg_date']; ?></td>
                            <!-- <td><?php
							if($data['verified'] == 1){
								echo 'App';
							} else{
								echo 'Web';
							}
							
							
							?></td> -->
                           
                            <td><?php 
                                if($data['login_date'] != ''){
                                    $date=date_create($data['login_date']);
                                    echo date_format($date,"M d, H:i a"); 
                                }
                                else{
                                    echo '-';
                                }
                                ?>
                            </td>
                            <td><?php 
                                if($data['logout_date'] != ''){
                                    $date=date_create($data['logout_date']);
                                    echo date_format($date,"M d, H:i a"); 
                                }
                                else{
                                    echo '-';
                                }
                                ?>
                            </td>
                          </tr>
                      <?php			
                        }
                      ?>
                  
                    </table>  
                </div>
            </div>   
            <nav>
              <ul class="pagination pagination-sm" id="pagination">
                <?php if(!empty($total_pages)):for($i=1; $i<=$total_pages; $i++):  
                            if($i == 1):?>
                     <li onClick="update(<?php echo $i;?>)" class="page-item <?php if($_POST['page'] == $i) echo 'active'; ?>" id="<?php echo $i;?>">
                      <a class="page-link" href="#" ><?php echo $i;?></a>
                    </li>
                <?php else:?>
                    <li onClick="update(<?php echo $i;?>)" class="page-item <?php if($_POST['page'] == $i) echo 'active'; ?>" id="<?php echo $i;?>">
                      <a class="page-link" href="#" ><?php echo $i;?></a>
                    </li>
                <?php endif;?>
                <?php endfor;endif;?>
              </ul>
            </nav>
			<?php
			break;
			
			      
   
	case 'getcomment':
    if (isset($_POST["page"])) 
    { 
        $page  = $_POST["page"]; 
    }
    else { 
        $page=1; 
    }
    
    $start_from = ($page-1) * $limit;

    $sql = "SELECT COUNT(id) as count FROM table_ques";  
    $rs_result = mysqli_query($link, $sql) or die(mysqli_error($link)); 
    $row = mysqli_fetch_assoc($rs_result);
    $total_records = $row['count'];  
    $total_pages = ceil($total_records / $limit);
    ?>
    <div class="row user-info">
        <div class="col-6">
            Total Users: <?php echo $total_records; ?>
        </div>
    </div> 
    <div class="row user-details">
        <div class="col-12">
            <table class="table table-striped table-light">
              <thead class="thead-inverse">
                <tr>
                 
                  <!-- <th>First name</th>
                  <th>Last name</th> -->
                  <th>Email</th>
                  <th>Comment</th>
                  <th>Asked at</th>
                  <!-- <th>specialty</th> -->
                  <!-- <th>Country</th>
                  <th>State</th>
                  <th>city</th> -->
                  <!-- <th>experience</th>
                  <th>Check box</th>
                  <th>Register on</th>
                  <th>Last Login On</th>
                  <th>Logout On</th> -->
                </tr>
              </thead>
              <tbody>
              <?php		
                $query="select * from table_ques ";
                $res = mysqli_query($link, $query) or die(mysqli_error($link));
                while($data = mysqli_fetch_assoc($res))
                {
                ?>
                  <tr>
                    <!-- <td><?php echo $data['title']; ?></td> -->
                    <td><?php echo $data['email']; ?></td>
                    <td><?php echo $data['user_question']; ?></td>
                    <td><?php echo $data['asked_at']; ?></td>
                    <!-- <td><?php echo $data['phone_num']; ?></td> -->
      
                    <!-- <td><?php echo $data['updates']; ?></td> -->
                    
      <!-- <td><?php echo $data['topic_interest']; ?></td> -->
      <!-- <td><?php echo $data['country']; ?></td>
      <td><?php echo $data['state']; ?></td>
      <td><?php echo $data['city']; ?></td> -->
                    <!-- <td><?php echo $data['specialty']; ?></td>
                    <td><?php echo $data['checked']; ?></td>

                    <td><?php echo $data['reg_date']; ?></td> -->
                    <!-- <td><?php
      if($data['verified'] == 1){
        echo 'App';
      } else{
        echo 'Web';
      }
      
      
      ?></td> -->
                   
                    <!-- <td><?php 
                        if($data['login_date'] != ''){
                            $date=date_create($data['login_date']);
                            echo date_format($date,"M d, H:i a"); 
                        }
                        else{
                            echo '-';
                        }
                        ?>
                    </td>
                    <td><?php 
                        if($data['logout_date'] != ''){
                            $date=date_create($data['logout_date']);
                            echo date_format($date,"M d, H:i a"); 
                        }
                        else{
                            echo '-';
                        }
                        ?>
                    </td> -->
                  </tr>
              <?php			
                }
              ?>
          
            </table>  
        </div>
    </div>   
    <nav>
      <ul class="pagination pagination-sm" id="pagination">
        <?php if(!empty($total_pages)):for($i=1; $i<=$total_pages; $i++):  
                    if($i == 1):?>
             <li onClick="update(<?php echo $i;?>)" class="page-item <?php if($_POST['page'] == $i) echo 'active'; ?>" id="<?php echo $i;?>">
              <a class="page-link" href="#" ><?php echo $i;?></a>
            </li>
        <?php else:?>
            <li onClick="update(<?php echo $i;?>)" class="page-item <?php if($_POST['page'] == $i) echo 'active'; ?>" id="<?php echo $i;?>">
              <a class="page-link" href="#" ><?php echo $i;?></a>
            </li>
        <?php endif;?>
        <?php endfor;endif;?>
      </ul>
    </nav>
<?php
break;
    
}

}
?>