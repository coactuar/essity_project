<!doctype html>
<html lang="en">
  <head>
    <title>Title</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <script src="https://code.jquery.com/jquery-3.5.0.js"></script>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  </head>
  <link href="https://fonts.googleapis.com/css?family=Roboto:300" rel="stylesheet">
<style>

body{
  font-family: 'arial', sans-serif;
  margin:0;
/* background-color:#003F63; */

}
form{
color:#f5ec00;
font-size:18px;
}
label{
color:black;
}
aside{
max-width:700px;
  margin:auto;
}
.survey-hr{
margin:30px 0;
  border: .5px solid #ddd;
}
i{
    color: black;
    /* background-color: black; */
    
}
/* input.largerCheckbox {
            width: 20px;
            height: 20px;
        } */

</style>
  <body>
  <section >


<aside style="padding:15px">

<nav class="navbar  navbar-light  ">

<img src="assects/img/AO_alliance_logo_col.png"  class=" w-25" alt="">

      
        <img src="assects/img/Essity _Logo.png"  class=" w-25" alt="">

</nav>  
<div class=" text-center mt-2">
                <img src="assects/img/Masterclass.png" class="" width="35%" alt=""/> 
            </div>
 <div id="question" class="mt-2">
              <div id="question-form" class="panel panel-default">
<form method="POST" action="#" class="form panel-body" role="form" >
<div id="ques-message"></div>
<label>Email Id</label><br>
<input type="email" name="email" class="form-control" placeholder="Enter your Email" id="" required>
<br>
<label>Sharing feedback for webinar attended (option to choose multiple choices)</label><br>

      <span class="">
<input type="checkbox" name="main[]"  value="Distal Radius Fracture: Focused Rigidity Casting Technique" ><i class=" ml-2 font-weight-bold ">Distal Radius Fracture: Focused Rigidity Casting Technique</i>
<br>
<input type="checkbox" name="main[]"  value="Pediatric Fractures - Conservative Management"><i class=" ml-2 font-weight-bold">Pediatric Fractures - Conservative Management</i>
<br>
<input type="checkbox" name="main[]"  value="Advanced Splinting Techniques in Fractures & Sprains"><i class=" ml-2 font-weight-bold">Advanced Splinting Techniques in Fractures & Sprains</i>
<br>
<input type="checkbox" name="main[]"  value="Heel Lock Technique in Ankle Sprain Management"><i class=" ml-2 font-weight-bold">Heel Lock Technique in Ankle Sprain Management</i>
</span>
<hr class="survey-hr"> 
<label>1. How would you rate the webinar overall??</label><br>

<span class="">
<input type="radio" name="rating"  value="Poor" required><i class=" ml-2">Poor</i>
<br>
<input type="radio" name="rating"  value="Fair" required><i class=" ml-2">Fair</i>
<br>
<input type="radio" name="rating"  value="Good" required><i class=" ml-2">Good</i>
<br>
<input type="radio" name="rating"  value="Excellent" required><i class=" ml-2">Excellent</i>
</span>
<hr class="survey-hr"> 
<div class="mt-5">

<label>2. How informative did you find the webinar? </label><br>
<span class="">
<input type="radio" name="rating1"  value="Not informative at all" required><i class=" ml-2">Not informative at all</i>
<br>
<input type="radio" name="rating1"  value="Not very informative" required><i class=" ml-2"> Not very informative</i>
<br>
<input type="radio" name="rating1"  value="Moderately informative" required><i class=" ml-2">Moderately informative</i>
<br>
<input type="radio" name="rating1"  value="Very informative" required><i class=" ml-2">Very informative</i>
<br>
<input type="radio" name="rating1"  value="Extremely informative" required><i class=" ml-2">Extremely informative</i>


</span>
</div>
<hr class="survey-hr"> 
<div class="mt-5">

<label>3. The length / pace of the webinar was </label><br>
<span class="">
<input type="radio" name="rating2"  value="Too long/slow" required><i class=" ml-2">Too long/slow</i>
<br>
<input type="radio" name="rating2"  value="Just right" required><i class=" ml-2"> Just right</i>
<br>
<input type="radio" name="rating2"  value="Too short/fast" required><i class=" ml-2">Too short/fast</i>


</span>
</div>
<hr class="survey-hr"> 
<div class="mt-5">

<label>4. How useful will be the presented ideas in your practice? </label><br>
<span class="">
<input type="radio" name="rating3"  value="Not useful at all" required><i class=" ml-2">Not useful at all</i>
<br>
<input type="radio" name="rating3"  value="Not very useful" required><i class=" ml-2"> Not very useful</i>
<br>
<input type="radio" name="rating3"  value="Moderately useful" required><i class=" ml-2">Moderately useful</i>
<br>
<input type="radio" name="rating3"  value="Very useful" required><i class=" ml-2">Very useful</i>
<br>
<input type="radio" name="rating3"  value="Extremely useful" required><i class=" ml-2"> Extremely useful</i>


</span>
<br>

</div>
<div class="clear"></div> 
<hr class="survey-hr"> 
<label for="">5. How can we improve webinars further or any other feedback you want to share?</label><br/><br/>
<textarea cols="75" name="commentText" rows="2" style="width:380px" required></textarea><br>
<br>
<span class="">
<input type="checkbox"   name="yes" value="yes" class="largerCheckbox"> <i class=" ml-2">I want to know more about Essity’s orthopedic solutions, please contact me.</i>
</span>

<div class="clear"></div> 
<br>
 <input style="background:#43a7d5;color:#fff;padding:12px;border:0" type="submit" class="" value="Submit">&nbsp; 

</form> 
              </div>
</div>
</aside>
</section> 

<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       
   <h4 class="text-success">Your Feedback is submitted successfully. <div id="ques-message"></div></h4>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
       
      </div>
    </div>
  </div>
</div>
<script src="assects/js/jquery.min.js"></script>
<script src="assects/js/bootstrap.min.js"></script>
<script>



$(document).on('submit', '#question-form form', function()
  {  

          $.post('feedbacksubmit.php', $(this).serialize(), function(data)
          {
            // alert("Your feedback is submitted successfully");
              if(data=="success")
              {
               
                $('#ques-message').text('Your Feedback is submitted successfully.');
                $('#ques-message').removeClass('alert-danger').addClass('alert-success').fadeIn().delay(2000).fadeOut();
                $('#question-form').find("textarea").val('');
                $("#exampleModalCenter").modal('show');

              }
              else 
              {
                $('#ques-message').text(data);
                $('#ques-message').removeClass('alert-success').addClass('alert-danger').fadeIn().delay(5000).fadeOut();
                $("#exampleModalCenter").modal('show');
              }
              
          });
      
    
    return false;
  });

</script>
    </body>
</html>