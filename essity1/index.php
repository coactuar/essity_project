
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title> Coact-live</title>
<link rel="stylesheet" href="assects/css/bootstrap.min.css">
<link rel="stylesheet" href="assects/css/all.min.css">
<link rel="stylesheet" href="assects/css/styles.css">

</head>

<body> 

	<div class="container">
    <nav class="navbar  navbar-light  ">

<img src="assects/img/AO_alliance_logo_col.png"  class=" w-25" alt="">

      
        <img src="assects/img/Essity _Logo.png"  class=" w-25" alt="">

</nav>
        <div class="row no-margin">
            <div class="col-12 text-center mt-2">
                <img src="assects/img/Masterclass.png" class="" width="35%" alt=""/> 
            </div>
        </div>
        <!-- <div class="row bg-white color-grey">
            <div class="col-12 text-center">
                 <h3 class="reg-title">Register for Coact </h3>
            </div>
        </div> -->
        <div class="row mt-3">
            <div class="col-12 col-md-6 ">
              
                <div id="register-area">
                 
                  <form method="POST" id="reg-form">
                  <!-- <h5> <div id="login-message"></div></h5> -->
                  <input type="hidden" id="app" name="app" value=""> 
                      <div class="row mt-2">
					  
                          <div class="col-12 col-md-6">
                          
                          <label for=""></label>
                              <input type="text" id="fname" name="fname" class="input" placeholder="First Name"  required>
                          </div>
                          <div class="col-12 col-md-6">
            <label for=""></label>
                              <input type="text" id="lname" name="lname" class="input" placeholder="Last Name"  required>
                          </div>
                      </div>
                  
                      <div class="row mt-1 mb-1">
                          <div class="col-12 col-md-6">
                          <label for=""></label>
                              <input type="email" id="emailid" name="emailid" class="input" placeholder="Email Id"   required>
                          </div>
						    <div class="col-12 col-md-6">
              <label for=""></label>
                              <input type="number" id="phone" name="phone" class="input" placeholder="Phone Number"   maxlength="10" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" required>
                          </div>
                      </div>
					   
                      <div class="row mt-1 mb-1">
                      <div class="col-12 col-md-6">
                      <label for=""></label>
                              <input type="text" id="specialty" name="specialty" class="input"  placeholder="Hospital/Organization Name " required>
                          </div>
                          
                          <div class="col-12 col-md-6">
                          <label for=""></label>
                              <select  id="pincode" name="select" class="input" value="" required>
                                  <option value="">Select Specialty </option>
                                  <!-- <option value="1">1</option> -->
                                  <option value="orthopedic surgeon">Orthopedic Surgeon</option>
                                  <option value="Pediatric orthopedic surgeon">Pediatric Orthopedic Surgeon</option>
                                  <option value="foot  ankle surgeon">Foot & Ankle Surgeon</option>
                                  <option value="plastic surgeon">Plastic Surgeon</option>
                                  <option value="pg student resident sr Resident">PG Student/Resident/Sr Resident</option>
                                  <option value="other">Other</option>
                              </select>
                                </div>
						 
                        
                      </div>

					 
					  <div class="row mt-1">
                          <div class="col-12 col-md-6">
                          <label for=""></label>
                              <div id="countries">
                              <select class="input" id="country" name="country" onChange="updateState()"  required>
                                  <option>Select Country</option>
                              </select>
                          </div> 
                          </div>
                          <div class="col-12 col-md-6">
                          <label for=""></label>
                              <div id="states">
                              <select class="input" id="state" name="state" onChange="updateCity()"  required>
                                  <option>Select State</option>
                                
                              </select>
                              </div>
                          </div>
                          </div>
                
					      <div class="row mt-1">
                          <div class="col-12 col-md-6">
                          <label for=""></label>
                              <div id="cities">
                              <select class="input" id="city" name="city" required>
                                  <option>Select City</option>
                              </select>
                              </div>
                          </div>
                          <div class="col-12 col-md-6">
                          <label for=""></label>
<select  id="pincode" name="exprience" class="input" value="" required>
    <option value="">No. of years’ experience</option>
    <!-- <option value="1">1</option> -->
    <option value="less than 5 years">less than 5 years</option>
    <option value="5 to 10 years">5 to 10 years</option>
    <option value="More than 10 years">More than 10 years</option>

</select>
  </div>

                      </div> 
                
                  
                      <label for=""></label>
  <input type="checkbox" id="vehicle2" name="check" value="Yes" >  Yes, I am in touch with Essity Sales Representative
  <!-- <label for="vehicle2" style="font-size: 13px;"></label> -->
                      <div class="row ">
                          <div class="col-12">
                              
                        <br>
                              <input type="submit" name="reguser-btn" id="btnSubmit" class="form-submit  bg-info " style="width: 15%; height:70%" value="Submit" />
                              <!-- <a href="./" class="form-cancel"><img src="assects/img/cancel-btn.jpg" alt=""/></a> -->
                          </div>
                      
                      </div>
                  </form>
                </div>
              </div>

        <div class="col-12 col-md-6 text-center mt-4 ">
<!--         
        <img src="assects/img/time.jpg" class=" " width="10%" alt="">
        <p class="font_change  text-center">5:00 PM - 6:00 PM </p> -->
       
        <p class="font_change ">Digital Masterclass Series on “Advances in Casting & Splinting” is created with the intention of spreading awareness about global best practices and supporting adoption of innovative techniques with the help of international faculty. The curriculum is co-created by top Indian and International faculty from Europe and Asia in partnership with AO Alliance. The webinars will focus on panel discussion around real case studies to capture practical insights from world’s leading experts, known for their contributions in fracture management. </p>
        
	</div>
    <div class="col-12 col-md-12 text-center  ">
        
        
        <p class="font_change  text-center"><img src="assects/img/clock.jpg" class="" width="8%" alt="" >&nbsp; &nbsp;  5:00 PM - 6:00 PM (IST)  Saturday</p>
       
          
	</div>
    <table class="mt-5 text-center table table-striped">
  <thead>
    <tr>
      <!-- <th scope="col">#</th> -->
      <th scope="col">Date</th>
      <th scope="col">Digital Masterclass</th>
      <!-- <th scope="col">Handle</th> -->
    </tr>
  </thead>
  <tbody>
    <tr >

      <td style="width: 30%;" >17th July</td>
      <td>Distal Radius Fracture: Focused Rigidity Casting Technique</td>
    </tr>
    <tr>

      <td>31st July</td>
      <td>Pediatric Fractures - Conservative Management</td>
    </tr>
    <tr>
     
      <td>14th Aug</td>
      <td>	
Advanced Splinting Techniques in Fractures & Sprains</td>
    </tr>
    <tr>
     
     <td>21st Aug</td>
     <td>Heel Lock Technique in Ankle Sprain Management</td>
   </tr>
  </tbody>
</table>
        </div>
<div class="row">
    <div class="col-12 col-md-12">
    <div id="question-form" class="panel panel-default">
			   <span style="color:black ; font-size:15px">Please submit your questions/feedback</span>
                  <form method="POST" action="#" class="form panel-body" role="form">
                      <div class="row">
                          <div class="col-12">
                          <div id="ques-message"></div>
                          <div class="form-group">
                             <input type="email"class="form-control" name="email" id="userQuestion" required placeholder="Email id" rows="1">
                          </div>
                          <div class="form-group">
                              <textarea class="form-control" name="userQuestion" id="userQuestion" required placeholder="Comment Box" rows="2"></textarea>
                          </div>
                          
                          </div>
                          <div class="col-12">
            
                          <button class="btn btn-primary btn-sm btn-submit" type="submit">Submit</button>
                          </div>
                      </div>
                      
                      
                      
                  </form>
              </div>
    <!-- <form id="#question-form" method="POST">
    <h4>Please submit your questions/feedback</h4>
  <div class="form-group">
    <label for="exampleFormControlInput1">Email address</label>
    <input type="email" class="form-control" id="exampleFormControlInput1"  placeholder="Email id" required>
  </div>
  <div class="form-group">
    <label for="Enter your text"> Comment Box</label>
    <textarea class="form-control" id="exampleFormControlTextarea1" placeholder="Comment Box" rows="3" required></textarea>
  </div>
  <input type="submit" class="btn-md bg-info" value="Submit">
</form> -->

    </div>
</div>

<div id="code"></div>

<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       
   <h4> <div id="login-message"></div></h4>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
       
      </div>
    </div>
  </div>
</div>

<script src="assects/js/jquery.min.js"></script>
<script src="assects/js/bootstrap.min.js"></script>

<script>

$(document).ready(function() {
    // alert($("#country").val());
    // alert($("#country").trigger("change").val());
    // var conceptName = $('#country').val();
    // alert(conceptName);
});

$(document).on('submit', '#reg-form', function()
{  
  $.post('functions/req1.php', $(this).serialize(), function(data)
  {
//     setTimeout(function(){
//    $('#btnSubmit').show();
// }, 5000);
$('#btnSubmit').fadeOut(); 
		   $('#btnSubmit').delay(5000).fadeIn();
      console.log(data);
      if(data == 's')
      {
        $('#login-message').text('You are registered succesfully for the Essity Masterclass . Please check your email regarding event Details.');
        $('#login-message').addClass('alert-success');
		$("#exampleModalCenter").modal('show');
	
        
          return false;
      }
      else if (data == '1')
      {
          $('#login-message').text('You are already registered.');
          $('#login-message').addClass('alert-danger');
		$("#exampleModalCenter").modal('show');
         
		//    $('#btnSubmit').fadeOut(); 
		//      $('#btnSubmit').delay(5000).fadeIn();
          return false;
      }
      else
      {

          $('#login-message').text(data);
          $('#login-message').addClass('alert-danger');
		  $("#exampleModalCenter").modal('show');  
		//   $('#btnSubmit').fadeOut(); 	  
		//   $('#btnSubmit').delay(5000).fadeIn(); 	  
          return false;
      }
  });
  
  return false;
});

$(function(){


$(document).on('submit', '#question-form form', function()
  {  
          $.post('submitques.php', $(this).serialize(), function(data)
          {
              if(data=="success")
              {
                $('#ques-message').text('Your Comment is submitted successfully.');
                $('#ques-message').removeClass('alert-danger').addClass('alert-success').fadeIn().delay(2000).fadeOut();
                $('#question-form').find("textarea").val('');
              }
              else 
              {
                $('#ques-message').text(data);
                $('#ques-message').removeClass('alert-success').addClass('alert-danger').fadeIn().delay(5000).fadeOut();
              }
              
          });
      
    
    return false;
  });
});
</script>
<script>
function getCountries()
{
    $.ajax({
        url: 'functions/server.php',
        data: {action: 'getcountries'},
        type: 'post',
        success: function(response) {
            
            $("#countries").html(response);
            $("#country").trigger("change");
        }
    });
   
}

function updateState()
{
    var c = $('#country').val();
    if(c!='0'){
        $.ajax({
            url: 'functions/server.php',
            data: {action: 'getstates', country : c },
            type: 'post',
            success: function(response) {
                
                $("#states").html(response);
            }
        });
    }
}

function updateCity()
{
    var s = $('#state').val();
    if(s!='0'){
        $.ajax({
            url: 'functions/server.php',
            data: {action: 'getcities', state : s },
            type: 'post',
            success: function(response) {
                
                $("#cities").html(response);
            }
        });
    }
}

getCountries();
//updateState();
</script>


</body>
</html>