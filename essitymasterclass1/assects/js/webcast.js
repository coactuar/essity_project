var updSes, sessionMsgs, getACount, getAtten, player;
$(function () {

    $(document).on('click', '.send_sesques', function () {
        //alert();
        var ques = $('#sesques').val();
        if (ques === '') {
            $('#ques-message').text('Please enter question').removeClass().addClass('alert alert-danger').fadeIn();
            return false;
        }
        var user = $(this).data('user');
        var ses = $(this).data('ses');

        $('.send_sesques').addClass('disabled');

        $.ajax({
            url: 'controls/server.php',
            data: { action: 'askquestion', ques: ques, userid: user, sessid: ses },
            type: 'post',
            success: function (response) {
                if (response === '1') {
                    $('#sesques').val('');
                    $('#ques-message').text('Your Question has been submitted successfully.').removeClass().addClass('alert alert-success').fadeIn().delay(3000).fadeOut();
                }
                else {
                    console.log(response);
                }

            }
        });

        $('.send_sesques').removeClass('disabled');
        return false;
    });

    $(document).on('click', '.send_takepoll', function () {
        var pollOpt = $("input[name='pollopts']:checked").val();

        if (pollOpt) {
            var user = $(this).data('user');
            var poll = $(this).data('poll');
            $('.send_takepoll').addClass('disabled');

            $.ajax({
                url: 'controls/server.php',
                data: { action: 'submitPollResp', pollId: poll, userId: user, answer: pollOpt },
                type: 'post',
                success: function (response) {
                    if (response !== '0') {
                        $('#currpoll').css('display', 'none');
                        $('#poll-message').text('Thank you for taking the poll. Results will be shown in a while.').removeClass().addClass('alert alert-success').fadeIn().delay(2000).fadeOut();

                    }
                    else {
                        console.log(response);
                    }

                }
            });

            $('.send_takepoll').removeClass('disabled');
            return false;
        }
        else {
            $('#poll-message').text("Please select your response").removeClass().addClass("alert alert-danger alert-msg").fadeIn().delay(2000).fadeOut();
        }

    });

    $(document).on('click', '#audi_askques', function () {
        $('.poll').removeClass('show');
        $('.ques').toggleClass('show');
    });

    $(document).on('click', '#close_ques', function () {
        $('.ques').toggleClass('show');
    });

    $(document).on('click', '#audi_takepoll', function () {
        $('.ques').removeClass('show');
        $('.poll').toggleClass('show');
    });

    $(document).on('click', '#close_poll', function () {
        $('.poll').toggleClass('show');
    });



});


function updateSession(web) {
    $.ajax({
        url: 'controls/server.php',
        data: { action: 'update-session', sessId: web },
        type: 'post',
        success: function (output) {
            //getAttendeesCount(web);

            if (output === "0") {
                location.href = 'lobby.php';
            }
            setTimeout(function () { updateSession(web); }, 15000);
        }
    });
}

function getAttendees(web) {
    $.ajax({
        url: 'controls/server.php',
        data: { action: 'getattendees', sessId: web },
        type: 'post',
        success: function (output) {
            $('#ses-attendees').html(output);
        }
    });

}

function getAttendeesCount(web) {
    $.ajax({
        url: 'controls/server.php',
        data: { action: 'getattendeescount', sessId: web },
        type: 'post',
        success: function (output) {
            $('#ses-attendees-count').html(output);
            getAttendees(web);
        }
    });

}

function getSessions(audi, day, keyword) {
    $.ajax({
        url: 'controls/server.php',
        data: { action: 'getSessions', key: keyword, audiId: audi, day: day },
        type: 'post',
        success: function (response) {
            var d = '#' + day;
            $(d).html(response);

        }
    });

}

function showDay1(audi) {
    $('#webcast_sessions .tabs a').removeClass('active');
    getSessions(audi, 'day1', '');
    $('#tab-day1').addClass('active');
    $('#sessions-day1').css('display', 'block');
    $('#sessions-day2').css('display', 'none');
}

function showDay2(audi) {
    $('#webcast_sessions .tabs a').removeClass('active');
    getSessions(audi, 'day2', '')
    $('#tab-day2').addClass('active');
    $('#sessions-day2').css('display', 'block');
    $('#sessions-day1').css('display', 'none');
}