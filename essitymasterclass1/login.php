
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>EssityMasterclass</title>
<link rel="stylesheet" href="assects/css/bootstrap.min.css">
<link rel="stylesheet" href="assects/css/all.min.css">
<link rel="stylesheet" href="assects/css/styles.css">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

</head>

<body> 

	<div class="container">
    <nav class="navbar  navbar-light  ">

<img src="assects/img/AO_alliance_logo_col.png"  class=" w-25" alt="">

      
        <img src="assects/img/Essity _Logo.png"  class=" w-25" alt="">

</nav>
        <div class="row no-margin">
            <div class="col-12 text-center mt-2">
                <img src="assects/img/Masterclass.png" class="" width="35%" alt=""/> 
            </div>
        </div>
  
        <div class="row mt-5">
            <div class="col-12 col-md-6 mt-5 offset-md-4 ">
              
                <div id="register-area">
                 
                  <form method="POST" id="login-form">
                  
                      <div class="row ">
                      <!-- <div id="login-message"></div> -->
                          <div class="col-12 col-md-8">
                          <label for="">Kindly Enter Registered Email ID to proceed</label>
                              <input type="email" id="emailid" name="email" class="input" placeholder="Email Id"   required>
                          </div>
                          <div class="col-12 mt-2 ">
            
            <input type="submit" name="reguser-btn" id="btnSubmit" class="form-submit btn-primary  p-2   " value="Submit" />
            
        </div>
        <div class="mt-2 ml-3">
        <a href="index.php" >Click Here To Register</a>
        </div>
      
                      </div>
                    
                   
                      
                      </div>
                  </form>
                </div>
              </div>

              <table class="mt-5 text-center table table-striped">
  <thead>
    <tr>
      <!-- <th scope="col">#</th> -->
      <th scope="col">Date</th>
      <th scope="col">Digital Masterclass</th>
      <!-- <th scope="col">Handle</th> -->
    </tr>
  </thead>
  <tbody>
    <tr >

      <td style="width: 30%;" >17th July</td>
      <td>Distal Radius Fracture: Focused Rigidity Casting Technique</td>
    </tr>
    <tr>

      <td>31st July</td>
      <td>Pediatric Fractures - Conservative Management</td>
    </tr>
    <tr>
     
      <td>14th Aug</td>
      <td>	
Advanced Splinting Techniques in Fractures & Sprains</td>
    </tr>
    <tr>
     
     <td>21st Aug</td>
     <td>Heel Lock Technique in Ankle Sprain Management</td>
   </tr>
  </tbody>
</table>

              <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       
   <h4> <div id="login-message1"></div></h4>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
       
      </div>
    </div>
  </div>
</div>


 <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  
<script src="assects/js/jquery.min.js"></script>
<script src="assects/js/bootstrap.min.js"></script>
<script>
$(document).on('submit', '#login-form', function()
{  
  $.post('chkforlogin.php', $(this).serialize(), function(data)
  {
//     setTimeout(function(){
//    $('#btnSubmit').show();
// }, 5000);
// $('#btnSubmit').fadeOut(); 
// 		   $('#btnSubmit').delay(5000).fadeIn();
      console.log(data);
      if(data == 's')
      {
        window.location.href='webcast.php';  
      }
      else if (data == '-1')
      {
          $('#login-message').text('You are already registered.');
          $('#login-message').addClass('alert-danger');
		$("#exampleModalCenter").modal('show');
         
		//    $('#btnSubmit').fadeOut(); 
		//      $('#btnSubmit').delay(5000).fadeIn();
          return false;
      }
      else
      {

          $('#login-message').text(data);
          $('#login-message').addClass('alert-danger');
		alert("Your are not register"); 
		//   $('#btnSubmit').fadeOut(); 	  
		//   $('#btnSubmit').delay(5000).fadeIn(); 	  
          return false;
      }
  });
  
  return false;
});
</script>
<!-- <script>0
$(document).on('submit', '#login-form', function()
{  

  $.post('chkforlogin.php', $(this).serialize(), function(data)
  {
     // console.log(data);
      if(data == 's')
      {
        window.location.href='webcast.php';  
      }
      else if (data == '-1')
      {
        $("#exampleModalCenter").modal('show');
          $('#login-message1').text('You are already logged in. Please logout and try again.');
          $('#login-message1').addClass('alert-danger').fadeIn().delay(3000).fadeOut();
          return false;
      }
      else
      {
        $("#exampleModalCenter").modal('show');
        // alert("hello");
          $('#login-message1').text(data);
          $('#login-message1').addClass('alert-danger').fadeIn().delay(3000).fadeOut();
          return false;
      }
  });
  
  return false;
});
</script> -->


</body>
</html>