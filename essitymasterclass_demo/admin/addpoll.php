<?php
	require_once "../config.php";
	
	// if(!isset($_SESSION["admin_user"]))
	// {
	// 	header("location: index.php");
	// 	exit;
	// }
	
	// if(isset($_GET['action']) && !empty($_GET['action'])) 
  //   {
  //       $action = $_GET['action'];
  //       if($action == "logout")
  //       {
  //           unset($_SESSION["admin_user"]);
            
  //           header("location: index.php");
  //           exit;
  //       }

  //   }
	
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Add Poll Question</title>
<link rel="stylesheet" type="text/css" href="../assects/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="../assects/css/all.min.css">
<link rel="stylesheet" type="text/css" href="../assects/css/styles.css">

</head>

<body class="admin">
<nav class="navbar navbar-expand-lg navbar-light">
   -->
  <div class="" id="navbarSupportedContent">
  </div>
</nav>
<div class="container-fluid">
    <div class="row login-info links">   
        <div class="col-8 text-left">
<a href="polls.php">Polls</a>
        </div>
        <li class="nav-item active">
         <a class="nav-link" href="users.php">Registered Users</a>
      </li>
    
      <li class="nav-item active">
         <a class="nav-link" href="comments.php">Comments Session</a>
      </li>
      <li class="nav-item active">
         <a class="nav-link" href="webquestion.php">Question</a>
      </li>
      <li class="nav-item active">
         <a class="nav-link" href="feedback.php">feedback</a>
      </li>
    
        <!-- <div class="col-4 text-right">
            <a href="#">Hello, <?php echo $_SESSION["admin_user"]; ?>!</a> <a href="?action=logout">Logout</a>
        </div> -->
    </div>
   <div class="row mt-2">
        <div class="col-12 col-md-8 offset-md-2">
            <h6>Add Poll Question</h6>
            <div id="poll-message"></div>
            <form id="add-poll">
              <div class="form-group">
                <label for="pollques">Poll Question</label>
                <textarea class="form-control" id="pollques" name="pollques" rows="3" required></textarea>
              </div>
              <div class="row">
                <div class="col-12 col-md-6">
                    <div class="form-group">
                      <label for="opt1">Option 1</label>
                      <input type="text" class="form-control" id="opt1" name="opt1" required>
                    </div>
                </div>
                <div class="col-12 col-md-6">
                    <div class="form-group">
                      <label for="opt2">Option 2</label>
                      <input type="text" class="form-control" id="opt2" name="opt2" required>
                    </div>
                </div>
              </div>
              <div class="row">
                <div class="col-12 col-md-6">
                    <div class="form-group">
                      <label for="opt3">Option 3</label>
                      <input type="text" class="form-control" id="opt3" name="opt3" required>
                    </div>
                </div>
                <div class="col-12 col-md-6">
                    <div class="form-group">
                      <label for="opt4">Option 4</label>
                      <input type="text" class="form-control" id="opt4" name="opt4" required>
                    </div>
                </div>
              </div>
              <div class="row">
                <div class="col-12 col-md-6">
                    <div class="form-group">
                      <label for="corrans">Correct Answer</label>
                      <select id="corrans" name="corrans" required class="form-control">
                          <option value="0">Select Correct Answer</option>
                          <option value="opt1">Option 1</option>
                          <option value="opt2">Option 2</option>
                          <option value="opt3">Option 3</option>
                          <option value="opt4">Option 4</option>
                      </select>
                    </div>
                </div>
              </div>
              <div class="row">
                <div class="col-12 col-md-6">
                    <div class="form-group">
                        <label for="">&nbsp;</label>
                      <button type="submit" class="btn btn-primary btn-sm">Add Poll Question</button>
                    </div>
                </div>
              </div>
            </form>
        </div>
   </div>
    
</div>
<script src="../assects/js/jquery.min.js"></script>
<script src="../assects/js/bootstrap.min.js"></script>
<script>
$(function(){
    
	$(document).on('submit', '#add-poll', function()
    {
      if($('#corrans').val() == '0')
      {
          alert('Select Correct Answer');
          return false;
      }  
      $.post('addpollques.php', $(this).serialize(), function(data)
      {
          if(data=="success")
          {
            location.href ="polls.php";
          }
          else 
          {
            $('#poll-message').text(data);
            $('#poll-message').removeClass('alert-success').addClass('alert-danger').fadeIn().delay(5000).fadeOut();
          }
          
      });
      
      return false;
    });
});
</script>
</body>
</html>