<?php
	require_once "../config.php";
	
	// if(!isset($_SESSION["admin_user"]))
	// {
	// 	header("location: index.php");
	// 	exit;
	// }
	
	// if(isset($_GET['action']) && !empty($_GET['action'])) 
    // {
    //     $action = $_GET['action'];
    //     if($action == "logout")
    //     {
    //         unset($_SESSION["admin_user"]);
            
    //         header("location: index.php");
    //         exit;
    //     }

    // }
	
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Polls</title>
<link rel="stylesheet" type="text/css" href="../assects/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="../assects/css/all.min.css">
<link rel="stylesheet" type="text/css" href="../assects/css/styles.css">

</head>

<body class="admin">
<!-- <nav class="navbar navbar-expand-lg navbar-light">
  <a class="navbar-brand" href="#"><img src="../img/logo.png" class="logo"></a>
  <div class="" id="navbarSupportedContent">
  </div>
</nav> -->
<div class="container-fluid">
    <div class="row login-info links">   
        <!-- <div class="col-8 text-left">
      <a href="polls.php">Polls</a>
        </div> -->
        <li class="nav-item active">
         <a class="nav-link" href="polls.php">Polls</a>
      </li>
        <li class="nav-item active">
         <a class="nav-link" href="users.php">Registered Users</a>
      </li>
    
      <li class="nav-item active">
         <a class="nav-link" href="comments.php">Comments Session</a>
      </li>
      <li class="nav-item active">
         <a class="nav-link" href="webquestion.php">Question</a>
      </li>
      <li class="nav-item active">
         <a class="nav-link" href="feedback.php">feedback</a>
      </li>
    
        <!-- <div class="col-4 text-right">
            <a href="#">Hello, <?php echo $_SESSION["admin_user"]; ?>!</a> <a href="?action=logout">Logout</a>
        </div> -->
    </div>
   <div class="row mt-2 p-2">
        <div class="col-12">
            <div id="poll"> </div>
        </div>
   </div>
    
</div>
<script src="../assects/js/jquery.min.js"></script>
<script src="../assects/js/bootstrap.min.js"></script>
<script>
$(function(){
getPolls('1');
});

function update(pageNum)
{
  getPolls(pageNum);
}

function getPolls(pageNum)
{
    $.ajax({
        url: 'ajax.php',
        data: {action: 'getpolls', page: pageNum},
        type: 'post',
        success: function(response) {
            $("#poll").html(response);
            
        }
    });
    
}

function updatePoll(id, val)
{
    var pid = '#poll'+id;
    //var curval = $(pid).val(); 
    $.ajax({
        url: 'ajax.php?id=' + id,
         data: {action: 'updatepoll', val: val },
         type: 'post',
         success: function(output) {
             //alert(output);
             getPolls('1');
         }
   });   
}

function delPoll(id)
{
    if(confirm('Are you sure?')){
        $.ajax({
            url: 'ajax.php',
             data: {action: 'delpoll', poll: id },
             type: 'post',
             success: function(output) {
                 //alert(output);
                 getPolls('1');
             }
       });
    }
}

</script>
</body>
</html>