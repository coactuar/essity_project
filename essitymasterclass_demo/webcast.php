<?php
	require_once "config.php";
	
	if(!isset($_SESSION["emailid"]))
	{
		header("location: login.php");
		exit;
	}
	
	if(isset($_GET['action']) && !empty($_GET['action'])) 
    {
        $action = $_GET['action'];
        if($action == "logout")
        {
            $logout_date   = date('Y/m/d H:i:s');
            $user_email=$_SESSION["emailid"];
            
            $query="UPDATE tbl_users set logout_date='$logout_date' where emailid='$user_email'";
            $res = mysqli_query($link, $query) or die(mysqli_error($link));

            unset($_SESSION["emailid"]);
            
            header("location: login.php");
            exit;
        }

    }
	
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>EssityMasterclass  Webcast</title>
<link rel="stylesheet" type="text/css" href="assects/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="assects/css/styles.css">
</head>

<body>
<div class="container-fluid">
    <div class="row logo-nav">
        <div class="col-12 col-md-4">
            <img src="assects/img/Masterclass.png" class="img-fluid w-75 logo" alt=""/> 
        </div>
     
    </div>
    <div class="row login-info bg-info ">
        <div class="col-12 p-1 text-right">
          <!-- Hello, <?php echo $_SESSION['user_name']; ?>!  -->
          <a class="btn btn-sm btn-light mr-2" href="feedback.php">Feedback</a>
          <a class="btn btn-sm btn-light" href="?action=logout">Logout</a>
        </div> 
    </div>
    <div class="row mt-4">
      <div class="col-12 col-md-8">
            <div class="embed-responsive embed-responsive-16by9">
            <iframe src="video.php" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen style="position:absolute;top:0;left:0;width:100%;height:100%;"></iframe>
            </div>
        </div>


        <div class="col-12 mt-2 col-md-4">
<div class="text-center">
<button type="button" class="bg-info text-white " id="formButton">Ask a Question</button>
</div>
  
          <div id="question" class="mb-3">
              <div id="question-form" class="panel panel-default">
                  <form method="POST" id="form1" action="#" class="form panel-body" role="form">
                      <div class="row">
                          <div class="col-12 mt-2">
                          <div id="ques-message"></div>
                          <div class="form-group">
                              <textarea class="form-control" name="userQuestion" id="userQuestion" required placeholder="Please ask your question" rows="5"></textarea>
                      
                              <!-- <input type="hidden" id="emailid" name="emailid" > -->
                            </div>
                          
                          </div>
                          <div class="col-12">
                                     
                          <input type="hidden" id="emailid" name="email" value="<?php echo $_SESSION['emailid']; ?>">
                          <button class="btn bg-info btn-primary btn-sm btn-submit w-100 " type="submit">Submit your Question</button>
                          </div>
                      </div>
                </form>
              </div>
          </div>
        
          <!-- <img src="assects/img/Digital Masterclass 1.jpg" class="img-fluid" alt=""> -->
          <div id="polls" style="display:none;">
            <div class="row mt-2">
                <div class="col-12">
                    <iframe id="poll-question" src="#" width="100%" height="350" frameborder="0" scrolling="no"></iframe>
                </div>
            </div>    
            </div> 
    </div>
    
</div>

<script src="assects/js/jquery.min.js"></script>
<script src="assects/js/bootstrap.min.js"></script>

<script>
//         $("#form1").hide();
// $("#formButton").click(function(){

//         $("#form1").toggle();
//     });
$(function(){
    //$('#videolinks').html('If you are unable to watch video, <a href="#" onClick="changeVideo()">click here</a>');
	$(document).on('submit', '#question-form form', function()
    {  
            $.post('webcastquestion.php', $(this).serialize(), function(data)
            {
                if(data=="success")
                {
                  $('#ques-message').text('Your question is submitted successfully.');
                  $('#ques-message').removeClass('alert-danger').addClass('alert-success').fadeIn().delay(2000).fadeOut();
                  $('#question-form').find("textarea").val('');
                }
                else 
                {
                  $('#ques-message').text(data);
                  $('#ques-message').removeClass('alert-success').addClass('alert-danger').fadeIn().delay(5000).fadeOut();
                }
                
            });
        
      
      return false;
    });
});

function update()
{
    $.ajax({ url: 'ajax.php',
         data: {action: 'update'},
         type: 'post',
         success: function(output) {
         }
});
}
setInterval(function(){ update(); }, 30000);


function changeVideo()
{
    var wc = $('#webcast').attr("src");
    console.log(wc);
    if(wc == "video.php")
    {
        $('#webcast').attr("src","video_bkup.php");
    }
    else
    {
        $('#webcast').attr("src","video.php");
    }
}
function update()
{
    $.ajax({ url: 'ajax.php',
         data: {action: 'update'},
         type: 'post',
         success: function(output) {
			   /*if(output=="0")
			   {
				   location.href='index.php';
			   }*/
         }
});
}
setInterval(function(){ update(); }, 30000);

var pollTimer;
function chkPolls()
{
    $.ajax({ url: 'ajax.php',
         data: {action: 'chkpoll'},
         type: 'post',
         success: function(output) {
			   if(output != 0)
			   {    
                    if($('#poll-question').attr('src') != output)
                   {
                       $("#poll-question").attr("src", output);
                       $('#polls').css('display','');
                   }
			   }
               else{
                   $("#poll-question").attr("src", '#');
                   $('#polls').css('display','none');
               }
         }
    });
}
chkPolls();
pollTimer = setInterval(function(){ chkPolls(); }, 3000);
</script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-93480057-13"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-93480057-13');
</script>

</body>
</html>