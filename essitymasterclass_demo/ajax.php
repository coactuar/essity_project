<?php
require_once "config.php";

if(isset($_POST['action']) && !empty($_POST['action'])) {
    
    $action = $_POST['action'];
    
    switch($action) {
        
        case 'update' : 
		
        $email=$_SESSION["user_email"];
		
		$query="select logout_status,login_date from tbl_users where user_email='$email'";
        $res = mysqli_query($link, $query) or die(mysqli_error($link));
        $data = mysqli_fetch_assoc($res);

        $logout=$data['logout_status'];
        $login_time = $data['login_date'];

        if($logout=="1")
        {

          $logout_date  = date('Y/m/d H:i:s', time() + 30);
          
          $query="UPDATE tbl_users set logout_date='$logout_date' where user_email='$email'";
          $res = mysqli_query($link, $query) or die(mysqli_error($link));
          
        }
        
        else{
            
          $logout_date   = date('Y/m/d H:i:s');
          $query="UPDATE tbl_users set logout_date='$logout_date' where user_email='$email'";
          $res = mysqli_query($link, $query) or die(mysqli_error($link));
          
          echo "0";
        }

		break;
       
    case 'chkpoll':
      $query="select * from tbl_polls where active='1'";
      $res = mysqli_query($link, $query) or die(mysqli_error($link));
      
      if (mysqli_affected_rows($link) > 0) 
      {
          $data = mysqli_fetch_assoc($res);
          $id = $data['id'];
          echo 'poll.php?id='. $id;
      }
      else
      {
          echo '0';
      }
      
  
  break;
  
  case 'getpollresults':
  
      $pid = $_POST['pid'];
      
      $query = 'select * from tbl_pollanswers where poll_id = "'.$pid.'"  ';
      $res = mysqli_query($link, $query) or die(mysqli_error($link));
      $data = mysqli_fetch_assoc($res);
      $myans = $data['poll_answer'];
      
      
      $query = 'select count(*) as c from tbl_pollanswers where poll_id = "'.$pid.'"';
      $res = mysqli_query($link, $query) or die(mysqli_error($link));
      $data = mysqli_fetch_assoc($res);
      $total_count = $data['c'];
      
      $poll = 'select * from tbl_polls where id = "'.$pid.'"';
      $res = mysqli_query($link, $poll) or die(mysqli_error($link));
      $data = mysqli_fetch_assoc($res);
      $poll_ques = $data['poll_question'];
      $poll_opt1 = $data['poll_opt1'];
      $poll_opt2 = $data['poll_opt2'];
      $poll_opt3 = $data['poll_opt3'];
      $poll_opt4 = $data['poll_opt4'];
      $corrans = $data['correct_ans'];
      
      
      echo '<b>'.$poll_ques.'</b><br><br>';
      //echo 'A. '.$poll_opt1.' [';
      $query = 'select count(*) as c from tbl_pollanswers where poll_id = "'.$pid.'" and poll_answer="opt1" ';
      $res = mysqli_query($link, $query) or die(mysqli_error($link));
      $data = mysqli_fetch_assoc($res);
      $opt1_count = $data['c'];
      //echo $opt1_count . ']<br>';
      $opt1width = ($opt1_count/$total_count) * 100;
      $ans = '';
      if(($myans == 'opt1'))
      {
          if($corrans == 'opt1') 
          {$ans = 'corrans'; }
          else { $ans = 'wroans'; }
      }
      else
      if($corrans == 'opt1')
      {$ans = 'corrans'; }
      ?>
      <strong><?php echo $poll_opt1; ?></strong><span class="float-right"><?php echo round($opt1width,0); ?>%</span>
      <div class="progress <?php echo $ans; ?>">
        <div class="progress-bar bg-success" role="progressbar" style="width: <?php echo $opt1width; ?>%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
      </div>
      <?php
      
      //echo 'B. '.$poll_opt2.' [';
      $query = 'select count(*) as c from tbl_pollanswers where poll_id = "'.$pid.'" and poll_answer="opt2" ';
      $res = mysqli_query($link, $query) or die(mysqli_error($link));
      $data = mysqli_fetch_assoc($res);
      $opt2_count = $data['c'];
      //echo $opt2_count . ']<br>';
      $opt2width = ($opt2_count/$total_count) * 100;
      $ans ='';
      if(($myans == 'opt2'))
      {
          if($corrans == 'opt2') 
          {$ans = 'corrans'; }
          else { $ans = 'wroans'; }
      }
      else
      if($corrans == 'opt2')
      {$ans = 'corrans'; }
      ?>
      <strong><?php echo $poll_opt2; ?></strong><span class="float-right"><?php echo round($opt2width,0); ?>%</span>
      <div class="progress <?php echo $ans; ?>">
        <div class="progress-bar bg-info" role="progressbar" style="width: <?php echo $opt2width; ?>%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
      </div>
      <?php
      
      //echo 'C. '.$poll_opt3.' [';
      $query = 'select count(*) as c from tbl_pollanswers where poll_id = "'.$pid.'" and poll_answer="opt3"';
      $res = mysqli_query($link, $query) or die(mysqli_error($link));
      $data = mysqli_fetch_assoc($res);
      $opt3_count = $data['c'];
      //echo $opt3_count . ']<br>';
      $opt3width = ($opt3_count/$total_count) * 100;
      $ans = '';
      if(($myans == 'opt3'))
      {
          if($corrans == 'opt3') 
          {$ans = 'corrans'; }
          else { $ans = 'wroans'; }
      }
      else
      if($corrans == 'opt3')
      {$ans = 'corrans'; }
      ?>
      <strong><?php echo $poll_opt3; ?></strong><span class="float-right"><?php echo round($opt3width,0); ?>%</span>
      <div class="progress <?php echo $ans; ?>">
        <div class="progress-bar bg-warning" role="progressbar" style="width: <?php echo $opt3width; ?>%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
      </div>
      <?php
      //echo 'D. '.$poll_opt4.' [';
      $query = 'select count(*) as c from tbl_pollanswers where poll_id = "'.$pid.'" and poll_answer="opt4" ';
      $res = mysqli_query($link, $query) or die(mysqli_error($link));
      $data = mysqli_fetch_assoc($res);
      $opt4_count = $data['c'];
      //echo $opt4_count . ']<br>';
      $opt4width = ($opt4_count/$total_count) * 100;
      $ans = '';
      if(($myans == 'opt4'))
      {
          if($corrans == 'opt4') 
          {$ans = 'corrans'; }
          else { $ans = 'wroans'; }
      }
      else
      if($corrans == 'opt4')
      {$ans = 'corrans'; }
      ?>
      <strong><?php echo $poll_opt4; ?></strong><span class="float-right"><?php echo round($opt4width,0); ?>%</span>
      <div class="progress  <?php echo $ans; ?>">
        <div class="progress-bar bg-danger" role="progressbar" style="width: <?php echo $opt4width; ?>%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
      </div>
      <?php
      
      //echo '<br><b>Total Votes: </b>'.$total_count;
  
  break;


}
    }


?>