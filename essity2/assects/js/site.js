"use strict";

var script = $('script[src*=site]'); // or better regexp to get the file name..

var loc = script.attr('data-loc');
if (typeof loc === "undefined") {
    loc = '';
}


$(function () {
    updateEvent(loc);
    getPageAttendees(loc);

    //getLiveAttendees();
    //setInterval(function () { updateEvent(loc); }, 10000);
    //setInterval(function () { getLiveAttendees(loc); }, 5000);
    //setInterval(function () { getPageAttendees(loc); }, 4000);

    $('.hall-banner').magnificPopup({
        type: 'image',
        removalDelay: 300,
        mainClass: 'mfp-fade'
    });
    $('.hall-video').magnificPopup({
        type: 'iframe',
        iframe: {
            markup: '<div class="mfp-iframe-scaler">' +
                '<div class="mfp-close"></div>' +
                '<iframe class="mfp-iframe" frameborder="0" allowfullscreen></iframe>' +
                '<div class="mfp-title">Some caption</div>' +
                '</div>'
        },
        removalDelay: 300,
        mainClass: 'mfp-fade'



    });

});

function updateEvent(loc) {

    $.ajax({
        url: 'controls/server.php',
        data: { action: 'update-event', room: loc },
        type: 'post',
        success: function (output) {
            if (output === "0") {
                location.href = './';
            }

        }
    });

    setTimeout(function () { updateEvent(loc); }, 10000);

}

function getLiveAttendees(loc) {
    $.ajax({
        url: 'controls/server.php',
        data: { action: 'getliveatt', room: loc },
        type: 'post',
        success: function (output) {
            $('#total-att').html(output);
        }
    });
}

function getPageAttendees(loc) {
    $.ajax({
        url: 'controls/server.php',
        data: { action: 'getonpageatt', room: loc },
        type: 'post',
        success: function (output) {
            $('#page-att').html(output);
        }
    });

    getLiveAttendees();
    setTimeout(function () { getPageAttendees(loc); }, 15000);
}


