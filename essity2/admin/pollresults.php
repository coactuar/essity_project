<?php
require_once "../config.php";
// if(!isset($_SESSION["admin_user"]))
// 	{
// 		header("location: index.php");
// 		exit;
// 	}
	
// 	if(isset($_GET['action']) && !empty($_GET['action'])) 
//     {
//         $action = $_GET['action'];
//         if($action == "logout")
//         {
//             unset($_SESSION["admin_user"]);
            
//             header("location: index.php");
//             exit;
//         }

//     }
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Poll Results</title>
<link rel="stylesheet" type="text/css" href="../assects/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="../assects/css/all.min.css">
<link rel="stylesheet" type="text/css" href="../assects/css/styles.css">
</head>

<body class="admin">
<nav class="navbar navbar-expand-lg navbar-light">
  <a class="navbar-brand" href="#"><img src="../img/logo.png" class="logo"></a>
  <div class="" id="navbarSupportedContent">
  </div>
</nav>
<div class="container-fluid">
    <div class="row login-info links">   
        <div class="col-8 text-left">
            <a href="users.php">Users</a> | <a href="questions.php">Questions</a> | <a href="polls.php">Polls</a>
        </div>
        <!-- <div class="col-4 text-right">
            <a href="#">Hello, <?php echo $_SESSION["admin_user"]; ?>!</a> <a href="?action=logout">Logout</a>
        </div> -->
    </div>
    <div class="row pollques mt-2 p-2">
        <div class="col-12 col-md-6 p-1 border">
            <h6>Poll Results</h6>
            <form>
            <div id="pollresults"></div>
            </form>
        </div>
        <div class="col-12 col-md-3 p-1 border">
            <h6>Poll Times</h6>
            <form>
            <div id="polltimes"></div>
            </form>
        </div>
        <div class="col-12 col-md-3 p-1 border">
            <h6>Poll Scores</h6>
            <form>
            <div id="pollscores"></div>
            </form>
        </div>
        
     </div> 
     
</div>
<script src="../assects/js/jquery.min.js"></script>
<script src="../assects/js/bootstrap.min.js"></script>
<script language="javascript">
$(function(){
    getPollResults("<?php echo $_GET['id']; ?>");
    getPollTimes("<?php echo $_GET['id']; ?>");
    getPollScores();
});

function getPollResults(id)
{
    $.ajax({
        url: 'ajax.php',
        data: {action: 'getpollresults', pid: id},
        type: 'post',
        success: function(response) {
            
            $("#pollresults").html(response);
            
        }
    });
    
}
function getPollTimes(id)
{
    $.ajax({
        url: 'ajax.php',
        data: {action: 'getpolltimes', pid: id},
        type: 'post',
        success: function(response) {
            
            $("#polltimes").html(response);
            
        }
    });
    
}

function getPollScores()
{
    $.ajax({
        url: 'ajax.php',
        data: {action: 'getpollscores'},
        type: 'post',
        success: function(response) {
            
            $("#pollscores").html(response);
            
        }
    });
    
}


setInterval(function(){ getPollResults('<?php echo $_GET['id']; ?>'); }, 3000);
setInterval(function(){ getPollTimes('<?php echo $_GET['id']; ?>'); }, 5000);
setInterval(function(){ getPollScores(); }, 5000);
</script>
</body>
</html>