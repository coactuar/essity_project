<?php
require_once "../functions/config.php";
 
if(isset($_POST['action']) && !empty($_POST['action'])) {
    
  $action = $_POST['action'];
  
  switch($action) {
      
      
      case 'getusers':
      
          if (isset($_POST["page"])) 
          { 
              $page  = $_POST["page"]; 
          }
          else { 
              $page=1; 
          }
          
          $start_from = ($page-1) * $limit;
          $today=date('Y/m/d H:i:s');
          $sql = "SELECT COUNT(id) as count FROM tbl_users where  logout_date > '$today'";  
          $rs_result = mysqli_query($link, $sql) or die(mysqli_error($link)); 
          $row = mysqli_fetch_assoc($rs_result);
          $loggedin = $row['count'];
      
          
          $sql = "SELECT COUNT(id) as count FROM tbl_users ";  
          $rs_result = mysqli_query($link, $sql) or die(mysqli_error($link)); 
          $row = mysqli_fetch_assoc($rs_result);
          $total_records = $row['count'];  
          $total_pages = ceil($total_records / $limit);
          ?>
          <div class="row user-info">
              <div class="col-6">
                  Total Users: <?php echo $total_records; ?>
              </div>
              <div class="col-6">
                  Currently Logged In: <div id="logged-in"><?php echo $loggedin; ?></div>
              </div>
          </div> 
        
            <div class="row user-details">
                <div class="col-12">
                    <table class="table table-striped table-light">
                      <thead class="thead-inverse">
                        <tr>
                          <!-- <th>Title</th> -->
                          <th>First name</th>
                          <th>Last name</th>
                          <th>Email</th>
                          <th>Number</th>
                          <th>Hospital/Organization</th>
                          <th>specialty</th>
                          <!-- <th>Country</th>
                          <th>State</th>
                          <th>city</th> -->
                          <th>experience</th>
                          <th>Check box</th>
                          <th>Register on</th>
                          <th>Last Login On</th>
                          <th>Logout On</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php		
                        $query="select * from tbl_users ";
                        $res = mysqli_query($link, $query) or die(mysqli_error($link));
                        while($data = mysqli_fetch_assoc($res))
                        {
                        ?>
                          <tr>
                            <!-- <td><?php echo $data['title']; ?></td> -->
                            <td><?php echo $data['first_name']; ?></td>
                            <td><?php echo $data['last_name']; ?></td>
                            <td><?php echo $data['emailid']; ?></td>
                            <td><?php echo $data['phone_num']; ?></td>
							
                            <td><?php echo $data['updates']; ?></td>
                            
							<td><?php echo $data['topic_interest']; ?></td>
              <!-- <td><?php echo $data['country']; ?></td>
              <td><?php echo $data['state']; ?></td>
              <td><?php echo $data['city']; ?></td> -->
                            <td><?php echo $data['specialty']; ?></td>
                            <td><?php echo $data['checked']; ?></td>

                            <td><?php echo $data['reg_date']; ?></td>
                            <!-- <td><?php
							if($data['verified'] == 1){
								echo 'App';
							} else{
								echo 'Web';
							}
							
							
							?></td> -->
                           
                           <td><?php 
                                if($data['login_date'] != ''){
                                    $date=date_create($data['login_date']);
                                    echo date_format($date,"M d, H:i a"); 
                                }
                                else{
                                    echo '-';
                                }
                                ?>
                            </td>
                            <td><?php
                                $today=date("Y/m/d H:i:s");
        
                                $dateTimestamp1 = strtotime($data['logout_date']);
                                $dateTimestamp2 = strtotime($today);
                                //echo $row[5];
                                if ($dateTimestamp1 > $dateTimestamp2)
                                {
                                  echo "Logged In";
                                  //$loggedin += 1; 
                                }
                                else
                                { 
                                  if($data['logout_date'] != ''){
                                      $date=date_create($data['logout_date']);
                                      echo date_format($date,"M d, H:i a"); 
                                  }
                                  else{
                                      echo '-';
                                  }
                                  if($data['logout_status']=='1')
                                  {
                                    $ls="UPDATE tbl_users set logout_status='0' where id='".$data['id']."' ";
                                    $lsres = mysqli_query($link, $ls) or die(mysqli_error($link));
                                  }
                                }
                                ?>
                            </td>
                          </tr>
                      <?php			
                        }
                      ?>
                  
                    </table>  
                </div>
            </div>   
            <nav>
              <ul class="pagination pagination-sm" id="pagination">
                <?php if(!empty($total_pages)):for($i=1; $i<=$total_pages; $i++):  
                            if($i == 1):?>
                     <li onClick="update(<?php echo $i;?>)" class="page-item <?php if($_POST['page'] == $i) echo 'active'; ?>" id="<?php echo $i;?>">
                      <a class="page-link" href="#" ><?php echo $i;?></a>
                    </li>
                <?php else:?>
                    <li onClick="update(<?php echo $i;?>)" class="page-item <?php if($_POST['page'] == $i) echo 'active'; ?>" id="<?php echo $i;?>">
                      <a class="page-link" href="#" ><?php echo $i;?></a>
                    </li>
                <?php endif;?>
                <?php endfor;endif;?>
              </ul>
            </nav>
			<?php
			break;
			
			      
   
	case 'getcomment':
    if (isset($_POST["page"])) 
    { 
        $page  = $_POST["page"]; 
    }
    else { 
        $page=1; 
    }
    
    $start_from = ($page-1) * $limit;

    $sql = "SELECT COUNT(id) as count FROM table_ques";  
    $rs_result = mysqli_query($link, $sql) or die(mysqli_error($link)); 
    $row = mysqli_fetch_assoc($rs_result);
    $total_records = $row['count'];  
    $total_pages = ceil($total_records / $limit);
    ?>
    <div class="row user-info">
        <div class="col-6">
            Total Users: <?php echo $total_records; ?>
        </div>
    </div> 
    <div class="row user-details">
        <div class="col-12">
            <table class="table table-striped table-light">
              <thead class="thead-inverse">
                <tr>
                 
                  <!-- <th>First name</th>
                  <th>Last name</th> -->
                  <th>Email</th>
                  <th>Comment</th>
                  <th>Asked at</th>
                  <!-- <th>specialty</th> -->
                  <!-- <th>Country</th>
                  <th>State</th>
                  <th>city</th> -->
                  <!-- <th>experience</th>
                  <th>Check box</th>
                  <th>Register on</th>
                  <th>Last Login On</th>
                  <th>Logout On</th> -->
                </tr>
              </thead>
              <tbody>
              <?php		
                $query="select * from table_ques ";
                $res = mysqli_query($link, $query) or die(mysqli_error($link));
                while($data = mysqli_fetch_assoc($res))
                {
                ?>
                  <tr>
                    <!-- <td><?php echo $data['title']; ?></td> -->
                    <td><?php echo $data['email']; ?></td>
                    <td><?php echo $data['user_question']; ?></td>
                    <td><?php echo $data['asked_at']; ?></td>
                    <!-- <td><?php echo $data['phone_num']; ?></td> -->
      
                    <!-- <td><?php echo $data['updates']; ?></td> -->
                    
      <!-- <td><?php echo $data['topic_interest']; ?></td> -->
      <!-- <td><?php echo $data['country']; ?></td>
      <td><?php echo $data['state']; ?></td>
      <td><?php echo $data['city']; ?></td> -->
                    <!-- <td><?php echo $data['specialty']; ?></td>
                    <td><?php echo $data['checked']; ?></td>

                    <td><?php echo $data['reg_date']; ?></td> -->
                    <!-- <td><?php
      if($data['verified'] == 1){
        echo 'App';
      } else{
        echo 'Web';
      }
      
      
      ?></td> -->
                   
                    <!-- <td><?php 
                        if($data['login_date'] != ''){
                            $date=date_create($data['login_date']);
                            echo date_format($date,"M d, H:i a"); 
                        }
                        else{
                            echo '-';
                        }
                        ?>
                    </td>
                    <td><?php 
                        if($data['logout_date'] != ''){
                            $date=date_create($data['logout_date']);
                            echo date_format($date,"M d, H:i a"); 
                        }
                        else{
                            echo '-';
                        }
                        ?>
                    </td> -->
                  </tr>
              <?php			
                }
              ?>
          
            </table>  
        </div>
    </div>   
    <nav>
      <ul class="pagination pagination-sm" id="pagination">
        <?php if(!empty($total_pages)):for($i=1; $i<=$total_pages; $i++):  
                    if($i == 1):?>
             <li onClick="update(<?php echo $i;?>)" class="page-item <?php if($_POST['page'] == $i) echo 'active'; ?>" id="<?php echo $i;?>">
              <a class="page-link" href="#" ><?php echo $i;?></a>
            </li>
        <?php else:?>
            <li onClick="update(<?php echo $i;?>)" class="page-item <?php if($_POST['page'] == $i) echo 'active'; ?>" id="<?php echo $i;?>">
              <a class="page-link" href="#" ><?php echo $i;?></a>
            </li>
        <?php endif;?>
        <?php endfor;endif;?>
      </ul>
    </nav>
<?php
break;
    

		      
   
case 'getquestion':
  if (isset($_POST["page"])) 
  { 
      $page  = $_POST["page"]; 
  }
  else { 
      $page=1; 
  }
  
  $start_from = ($page-1) * $limit;

  $sql = "SELECT COUNT(id) as count FROM table_question";  
  $rs_result = mysqli_query($link, $sql) or die(mysqli_error($link)); 
  $row = mysqli_fetch_assoc($rs_result);
  $total_records = $row['count'];  
  $total_pages = ceil($total_records / $limit);
  ?>
  <div class="row user-info">
      <div class="col-6">
          Total Users: <?php echo $total_records; ?>
      </div>
  </div> 
  <div class="row user-details">
      <div class="col-12">
          <table class="table table-striped table-light">
            <thead class="thead-inverse">
              <tr>
               
                <!-- <th>First name</th>
                <th>Last name</th> -->
                <th>Email</th>
                <th>Question</th>
                <th>Asked at</th>
                <!-- <th>specialty</th> -->
                <!-- <th>Country</th>
                <th>State</th>
                <th>city</th> -->
                <!-- <th>experience</th>
                <th>Check box</th>
                <th>Register on</th>
                <th>Last Login On</th>
                <th>Logout On</th> -->
              </tr>
            </thead>
            <tbody>
            <?php		
              $query="select * from table_question ";
              $res = mysqli_query($link, $query) or die(mysqli_error($link));
              while($data = mysqli_fetch_assoc($res))
              {
              ?>
                <tr>
                  <!-- <td><?php echo $data['title']; ?></td> -->
                  <td><?php echo $data['email']; ?></td>
                  <td><?php echo $data['user_question']; ?></td>
                  <td><?php echo $data['asked_at']; ?></td>
                  <!-- <td><?php echo $data['phone_num']; ?></td> -->
    
                  <!-- <td><?php echo $data['updates']; ?></td> -->
                  
    <!-- <td><?php echo $data['topic_interest']; ?></td> -->
    <!-- <td><?php echo $data['country']; ?></td>
    <td><?php echo $data['state']; ?></td>
    <td><?php echo $data['city']; ?></td> -->
                  <!-- <td><?php echo $data['specialty']; ?></td>
                  <td><?php echo $data['checked']; ?></td>

                  <td><?php echo $data['reg_date']; ?></td> -->
                  <!-- <td><?php
    if($data['verified'] == 1){
      echo 'App';
    } else{
      echo 'Web';
    }
    
    
    ?></td> -->
                 
                  <!-- <td><?php 
                      if($data['login_date'] != ''){
                          $date=date_create($data['login_date']);
                          echo date_format($date,"M d, H:i a"); 
                      }
                      else{
                          echo '-';
                      }
                      ?>
                  </td>
                  <td><?php 
                      if($data['logout_date'] != ''){
                          $date=date_create($data['logout_date']);
                          echo date_format($date,"M d, H:i a"); 
                      }
                      else{
                          echo '-';
                      }
                      ?>
                  </td> -->
                </tr>
            <?php			
              }
            ?>
        
          </table>  
      </div>
  </div>   
  <nav>
    <ul class="pagination pagination-sm" id="pagination">
      <?php if(!empty($total_pages)):for($i=1; $i<=$total_pages; $i++):  
                  if($i == 1):?>
           <li onClick="update(<?php echo $i;?>)" class="page-item <?php if($_POST['page'] == $i) echo 'active'; ?>" id="<?php echo $i;?>">
            <a class="page-link" href="#" ><?php echo $i;?></a>
          </li>
      <?php else:?>
          <li onClick="update(<?php echo $i;?>)" class="page-item <?php if($_POST['page'] == $i) echo 'active'; ?>" id="<?php echo $i;?>">
            <a class="page-link" href="#" ><?php echo $i;?></a>
          </li>
      <?php endif;?>
      <?php endfor;endif;?>
    </ul>
  </nav>
<?php
break;
  		      
   
case 'getfeedback':
  if (isset($_POST["page"])) 
  { 
      $page  = $_POST["page"]; 
  }
  else { 
      $page=1; 
  }
  
  $start_from = ($page-1) * $limit;

  $sql = "SELECT COUNT(id) as count FROM table_feed";  
  $rs_result = mysqli_query($link, $sql) or die(mysqli_error($link)); 
  $row = mysqli_fetch_assoc($rs_result);
  $total_records = $row['count'];  
  $total_pages = ceil($total_records / $limit);
  ?>
  <div class="row user-info">
      <div class="col-6">
          Total Users: <?php echo $total_records; ?>
      </div>
  </div> 
  <div class="row user-details">
      <div class="col-12">
          <table class="table table-striped table-light">
            <thead class="thead-inverse">
              <tr>
               
                <!-- <th>First name</th>
                <th>Last name</th> -->
                <th>Email</th>
                <th>Digital</th>
                <th>question 1</th>
                <th>question 2</th>
                <th>question 3</th>
                <th>question 4</th>
                <th>Open text</th>
 <th>Check box</th>

                <!-- <th>specialty</th> -->
                <!-- <th>Country</th>
                <th>State</th>
                <th>city</th> -->
                <!-- <th>experience</th>
                <th>Check box</th>
                <th>Register on</th>
                <th>Last Login On</th>
                <th>Logout On</th> -->
              </tr>
            </thead>
            <tbody>
            <?php		
              $query="select * from table_feed ";
              $res = mysqli_query($link, $query) or die(mysqli_error($link));
              while($data = mysqli_fetch_assoc($res))
              {
              ?>
                <tr>
                  <!-- <td><?php echo $data['title']; ?></td> -->
                  <td><?php echo $data['email']; ?></td>
                  <td><?php echo $data['star']; ?></td>
                  <td><?php echo $data['radio']; ?></td>
                  <td><?php echo $data['radio_1']; ?></td>
    
                  <td><?php echo $data['radio_2']; ?></td>
                  
    <td><?php echo $data['radio_3']; ?></td>
    <td><?php echo $data['question']; ?></td>
 <td><?php echo $data['checked']; ?></td> 
    <td><?php echo $data['date']; ?></td>
 
                  <!-- <td><?php echo $data['specialty']; ?></td>
                  <td><?php echo $data['checked']; ?></td>

                  <td><?php echo $data['reg_date']; ?></td> -->
                  <!-- <td><?php
    if($data['verified'] == 1){
      echo 'App';
    } else{
      echo 'Web';
    }
    
    
    ?></td> -->
                 
                  <!-- <td><?php 
                      if($data['login_date'] != ''){
                          $date=date_create($data['login_date']);
                          echo date_format($date,"M d, H:i a"); 
                      }
                      else{
                          echo '-';
                      }
                      ?>
                  </td>
                  <td><?php 
                      if($data['logout_date'] != ''){
                          $date=date_create($data['logout_date']);
                          echo date_format($date,"M d, H:i a"); 
                      }
                      else{
                          echo '-';
                      }
                      ?>
                  </td> -->
                </tr>
            <?php			
              }
            ?>
        
          </table>  
      </div>
  </div>   
  <nav>
    <ul class="pagination pagination-sm" id="pagination">
      <?php if(!empty($total_pages)):for($i=1; $i<=$total_pages; $i++):  
                  if($i == 1):?>
           <li onClick="update(<?php echo $i;?>)" class="page-item <?php if($_POST['page'] == $i) echo 'active'; ?>" id="<?php echo $i;?>">
            <a class="page-link" href="#" ><?php echo $i;?></a>
          </li>
      <?php else:?>
          <li onClick="update(<?php echo $i;?>)" class="page-item <?php if($_POST['page'] == $i) echo 'active'; ?>" id="<?php echo $i;?>">
            <a class="page-link" href="#" ><?php echo $i;?></a>
          </li>
      <?php endif;?>
      <?php endfor;endif;?>
    </ul>
  </nav>
<?php
break;

case 'getpolls':
        
  if (isset($_POST["page"])) 
  { 
      $page  = $_POST["page"]; 
  }
  else { 
      $page=1; 
  }
  
  $start_from = ($page-1) * $limit;

  $sql = "SELECT COUNT(id) FROM tbl_polls";  
  $rs_result = mysqli_query($link,$sql);  
  $row = mysqli_fetch_row($rs_result);  
  $total_records = $row[0];  
  $total_pages = ceil($total_records / $limit);
  ?>
  <div class="row user-info">
      <div class="col-6">
          Total Poll Questions: <div id="ques_count"><?php echo $total_records; ?></div>
      </div>
      <div class="col-6 text-right">
          <a class="btn btn-sm btn-primary add-poll" href="addpoll.php">Add Poll</a>
      </div>
      
  </div> 
  <div class="row user-details">
      <div class="col-12">
          <table class="table table-dark table-striped">
            <thead class="thead-inverse">
              <tr>
                <th>Poll Question</th>
                <th></th>
                <th></th>
                <th></th>
              </tr>
            </thead>
            <tbody>
            <?php		
              $query="select * from tbl_polls order by poll_over, id asc LIMIT $start_from, $limit";
              $res = mysqli_query($link, $query) or die(mysqli_error($link));
              while($data = mysqli_fetch_assoc($res))
              {
              ?>
                <tr>
                  <td><?php echo $data['poll_question']; ?></td>
                  <td width="100">
                  <?php
                      if(!$data['poll_over'])
                      {
                          ?>
                          <a href="#" onClick="updatePoll('<?php echo $data['id']; ?>','<?php echo $data['active']; ?>')" class="btn btn-sm <?php if ($data['active'] == '1') { echo 'btn-danger'; } else echo 'btn-success';  ?>"><?php if ($data['active'] == '1') { echo 'Deactivate'; } else echo 'Activate';  ?> Poll</a>
                          
<!--                                   <input type="checkbox" id="poll<?php echo $data['id']; ?>" name="poll<?php echo $data['id']; ?>" value="<?php echo $data['active']; ?>" onClick="updatePoll('<?php echo $data['id']; ?>')" <?php if ($data['active'] == '1') echo 'checked';  ?> <?php if ($data['poll_over'] == '1') echo 'readonly';  ?>> 
-->                                    <?php
                      }
                  ?>
                  </td>
                  <td width="100"><a href="pollresults.php?id=<?php echo $data['id']; ?>" class="btn btn-sm btn-success">View Results</a></td>
                  <td width="10"><a href="#" class="btn btn-sm btn-danger" onClick="delPoll('<?php echo $data['id']; ?>')">Delete Poll</a></td>
                </tr>
            <?php			
              }
            ?>
        
          </table>  
      </div>
  </div>   
  <nav>
    <ul class="pagination pagination-sm" id="pagination">
      <?php if(!empty($total_pages)):for($i=1; $i<=$total_pages; $i++):  
                  if($i == 1):?>
           <li onClick="update(<?php echo $i;?>)" class="page-item <?php if($_POST['page'] == $i) echo 'active'; ?>" id="<?php echo $i;?>">
            <a class="page-link" href="#" ><?php echo $i;?></a>
          </li>
      <?php else:?>
          <li onClick="update(<?php echo $i;?>)" class="page-item <?php if($_POST['page'] == $i) echo 'active'; ?>" id="<?php echo $i;?>">
            <a class="page-link" href="#" ><?php echo $i;?></a>
          </li>
      <?php endif;?>
      <?php endfor;endif;?>
    </ul>
  </nav>
  <?php

  
break;

case 'updatepoll':
  
  $newval = 0;
  if($_POST['val'] == 0)
  {
      $newval = 1;
  }
  else
  {
      $newval = 0;
  }
  
  $sql = "Update tbl_polls set active ='0' ";  
  $rs_result = mysqli_query($link,$sql);
  
  if($newval){
  $sql = "Update tbl_polls set active ='$newval' where  id = '".$_GET['id']."'";  
  $rs_result = mysqli_query($link,$sql); 
  }
  
  
break;

case 'getpollresults':

  $pid = $_POST['pid'];
  
  $query = 'select count(*) as c from tbl_pollanswers where poll_id = "'.$pid.'"';
  $res = mysqli_query($link, $query) or die(mysqli_error($link));
  $data = mysqli_fetch_assoc($res);
  $total_count = $data['c'];
  
  $poll = 'select * from tbl_polls where id = "'.$pid.'" ';
  $res = mysqli_query($link, $poll) or die(mysqli_error($link));
  $data = mysqli_fetch_assoc($res);
  $poll_ques = $data['poll_question'];
  $poll_opt1 = $data['poll_opt1'];
  $poll_opt2 = $data['poll_opt2'];
  $poll_opt3 = $data['poll_opt3'];
  $poll_opt4 = $data['poll_opt4'];
  $corrans = $data['correct_ans'];
  
  echo '<b>'.$poll_ques.'</b><br><br>';
  //echo 'A. '.$poll_opt1.' [';
  $query = 'select count(*) as c from tbl_pollanswers where poll_id = "'.$pid.'" and poll_answer="opt1" ';
  $res = mysqli_query($link, $query) or die(mysqli_error($link));
  $data = mysqli_fetch_assoc($res);
  $opt1_count = $data['c'];
  //echo $opt1_count . ']<br>';
  $opt1width = 0;
  if($total_count != '0')
  {
      $opt1width = ($opt1_count/$total_count) * 100;
  }
  $ans = '';
  if($corrans == 'opt1')
  {$ans = 'corrans'; }
  ?>
  <strong><?php echo $poll_opt1; ?></strong><span class="float-right"><?php echo $opt1_count .'('.round($opt1width,0).'%)'; ?></span>
  <div class="progress <?php echo $ans; ?>">
    <div class="progress-bar progress-bar-striped progress-bar-animated bg-success" role="progressbar" style="width: <?php echo $opt1width; ?>%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
  </div>
  <?php
  
  //echo 'B. '.$poll_opt2.' [';
  $query = 'select count(*) as c from tbl_pollanswers where poll_id = "'.$pid.'" and poll_answer="opt2"';
  $res = mysqli_query($link, $query) or die(mysqli_error($link));
  $data = mysqli_fetch_assoc($res);
  $opt2_count = $data['c'];
  //echo $opt2_count . ']<br>';
  $opt2width = 0;
  if($total_count != '0')
  {
      $opt2width = ($opt2_count/$total_count) * 100;
  }
  $ans = '';
  if($corrans == 'opt2')
  {$ans = 'corrans'; }
  ?>
  <strong><?php echo $poll_opt2; ?></strong><span class="float-right"><?php echo $opt2_count .'('.round($opt2width,0).'%)'; ?></span>
  <div class="progress <?php echo $ans; ?>">
    <div class="progress-bar progress-bar-striped progress-bar-animated bg-info" role="progressbar" style="width: <?php echo $opt2width; ?>%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
  </div>
  <?php
  
  //echo 'C. '.$poll_opt3.' [';
  $query = 'select count(*) as c from tbl_pollanswers where poll_id = "'.$pid.'" and poll_answer="opt3"';
  $res = mysqli_query($link, $query) or die(mysqli_error($link));
  $data = mysqli_fetch_assoc($res);
  $opt3_count = $data['c'];
  //echo $opt3_count . ']<br>';
  $opt3width = 0;
  if($total_count != '0')
  {
      $opt3width = ($opt3_count/$total_count) * 100;
  }$ans = '';
  if($corrans == 'opt3')
  {$ans = 'corrans'; }
  ?>
  <strong><?php echo $poll_opt3; ?></strong><span class="float-right"><?php echo $opt3_count .'('.round($opt3width,0).'%)'; ?></span>
  <div class="progress <?php echo $ans; ?>">
    <div class="progress-bar progress-bar-striped progress-bar-animated bg-warning" role="progressbar" style="width: <?php echo $opt3width; ?>%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
  </div>
  <?php
  //echo 'D. '.$poll_opt4.' [';
  $query = 'select count(*) as c from tbl_pollanswers where poll_id = "'.$pid.'" and poll_answer="opt4" ';
  $res = mysqli_query($link, $query) or die(mysqli_error($link));
  $data = mysqli_fetch_assoc($res);
  $opt4_count = $data['c'];
  //echo $opt4_count . ']<br>';
  $opt4width = 0;
  if($total_count != '0')
  {
      $opt4width = ($opt4_count/$total_count) * 100;
  }$ans = '';
  if($corrans == 'opt4')
  {$ans = 'corrans'; }
  ?>
  <strong><?php echo $poll_opt4; ?></strong><span class="float-right"><?php echo $opt4_count .'('.round($opt4width,0).'%)'; ?></span>
  <div class="progress <?php echo $ans; ?>">
    <div class="progress-bar progress-bar-striped progress-bar-animated bg-danger" role="progressbar" style="width: <?php echo $opt4width; ?>%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
  </div>
  <?php
  
  echo '<br><b>Total Votes: </b>'.$total_count;
  
  $query = 'select count(*) as c from tbl_pollanswers where poll_id = "'.$pid.'" and poll_answer = "'.$corrans.'" ';
  $res = mysqli_query($link, $query) or die(mysqli_error($link));
  $data = mysqli_fetch_assoc($res);
  $corrans_count = $data['c'];
  
  // echo '<br><b>Total Correct Answers: </b>'.$corrans_count;
  
  

break;

case 'getpolltimes':
  $pid = $_POST['pid'];
  $poll = 'select * from tbl_polls where id = "'.$pid.'" ';
  $res = mysqli_query($link, $poll) or die(mysqli_error($link));
  $data = mysqli_fetch_assoc($res);
  $corrans = $data['correct_ans'];

  $query = 'select * from tbl_pollanswers, tbl_users where poll_id = "'.$pid.'" and poll_answer = "'.$corrans.'" and tbl_pollanswers.users_id=tbl_users.id  order by poll_at asc limit 10';
  //echo $query;
  $res = mysqli_query($link, $query) or die(mysqli_error($link));
  ?>
  <table class="table table-dark table-striped">
            <thead class="thead-inverse">
              <tr>
                <th>Name</th>
                <th>Time</th>
              </tr>
            </thead>
            <tbody>
  <?php
  while ($data = mysqli_fetch_assoc($res))
  {
  ?>
  <tr>
    <td><?php echo $data['emailid']; ?></td>
    <td><?php 
        if($data['poll_at'] != ''){
            $date=date_create($data['poll_at']);
            echo date_format($date,"H:i:s"); 
        }
        else{
            echo '-';
        }
        ?>
    </td>
  </tr>
  <?php    
  }
  ?>
  </tbody>
  </table>
  <?php


break;

case 'getpollscores':
  
  $query = 'SELECT sum(points) as total, users_id, tbl_users.emailid FROM `tbl_pollanswers`, tbl_users where tbl_pollanswers.users_id = tbl_users.id GROUP BY users_id order by total DESC limit 10';
  //echo $query;
  $res = mysqli_query($link, $query) or die(mysqli_error($link));
  ?>
  <table class="table table-dark table-striped">
            <thead class="thead-inverse">
              <tr>
                <th>Name</th>
                <th>Total Score</th>
              </tr>
            </thead>
            <tbody>
  <?php
  while ($data = mysqli_fetch_assoc($res))
  {
  ?>
  <tr>
    <td><?php echo $data['emailid']; ?></td>
    <td><?php echo $data['total'];  ?>
    </td>
  </tr>
  <?php    
  }
  ?>
  </tbody>
  </table>
  <?php


break;

case 'delpoll':
    $poll = $_POST['poll'];
  
    $sql = "delete from tbl_pollanswers where poll_id='$poll' ";  
    $rs_result = mysqli_query($link,$sql);  

    $sql = "delete from tbl_polls where id='$poll' ";  
    $rs_result = mysqli_query($link,$sql);  
break;



}

}
?>